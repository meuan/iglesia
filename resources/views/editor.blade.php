@section('title', 'Criptas')
@section('content')
    {{-- <script src="https://cdn.ckeditor.com/ckeditor5/[version.number]/[distribution]/ckeditor.js"></scrip --}}t>
    <script src="https://cdn.ckeditor.com/4.17.2/standard/ckeditor.js"></script>
    
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
            <div class="title_left">
                <h3>Plain Page</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5   form-group pull-right top_search">
                    <div class="input-group">
                    <input type="text" class="form-control" placeholder="Buscar">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Aceptar</button>
                    </span>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="x_panel">
                        
                        <div class="x_content">
                            <textarea name="editor1"></textarea>

                           {{--  <div id="editor">This is some sample content.</div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@extends('layout.main')

@section('scripts')
    <script>
        CKEDITOR.replace( 'editor1');
        /* ClassicEditor.create( document.querySelector( '#editor' ))
        .then( editor => {
                console.log( editor );
        } )
        .catch( error => {
                console.error( error );
        } ); */
    </script>
@endsection