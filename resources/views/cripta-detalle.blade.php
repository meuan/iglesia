@section('title', 'Criptas')
@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left"> 
                    <h3>Cripta - {{ $cripta->codigo }}</h3>
                    <input type="hidden" id="criptaId" value="{{$cripta->id}}"/>
                </div>
    
                <div class="title_right">
                    <button class="btn btn-primary btn-round btnAgregar float-right" data-toggle="modal" data-target="#modalFormulario" onclick="abreModal()" >Agregar</button>                
                </div>
            </div>
    
            <div class="clearfix"></div>
    
            <div class="row">
                <div class="col-md-12 col-sm-12  ">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Plain Page</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li>
                                    <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Settings 1</a>
                                        <a class="dropdown-item" href="#">Settings 2</a>
                                    </div>
                                </li>
                                <li>
                                    <a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr style="text-align: center">
                                                <th onclick="ordenar('codigo')" id="codigo" >Código</th>
                                                <th onclick="ordenar('responsable')" id="responsable">Responsable</th>
                                                <th onclick="ordenar('precio')" id="precio">Precio</th>
                                                <th style="width: 40%" onclick="ordenar('descripcion')" id="descripcion">Descripción</th>
                                                <th onclick="ordenar('disponibles')" id="disponibles">Disponibles</th>
                                                <th style="width: 15%">Opciones</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody"></tbody>
                                    </table>
        
                                    <nav aria-label="Page navigation example" class="row" style="float:right;">
                                        <div class="pagination">
                                            <div class="table_info"> Mostrar: </div>
                                            <div style="width: 70px;">
                                                <select class="custom-select" id="show">
                                                    <option value="10" selected>10</option>
                                                    <option value="25">25</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                </select>
                                            </div>
                                            <div class="table_info" id="tableInfo"></div>
                                            <nav aria-label="Page navigation" id="paginacion"></nav>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        <div class="modal fade bd-example-modal-lg" id="modalFormulario" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="tituloModal"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
    
                    <div class="modal-body">
                        <div class="form-group col-md-4">
                            <label for="txtCodigo">Código</label>
                            <input class="form-control" placeholder="Código" id="txtCodigo" autocomplete="off">
                            <span id="errorCodigo" style="color:red; display:none;">Campo requerdo</span>
                        </div>
    
                        <div class="form-group col-md-4">
                            <label for="txtLugares">Lugares </label>
                            <input type="number" class="form-control" placeholder="Lugares" id="txtLugares" autocomplete="off">
                            <span id="errorLugares" style="color:red; display:none;">Campo requerdo</span>
                        </div>
    
                        <div class="form-group col-md-4">
                            <label for="txtPrecio">Precio </label>
                            <input data-validate-minmax="1" type="number" class="form-control" placeholder="Precio" id="txtPrecio" autocomplete="off">
                            <span id="errorPrecio" style="color:red; display:none;">Campo requerdo</span>
                        </div>
    
                        <div class="form-group col-md-12">
                            <label for="txtResponsable">Responsable</label>
                            <input class="form-control" placeholder="Responsable" id="txtResponsable" autocomplete="off">
                            <span id="errorResponsable" style="color:red; display:none;">Campo requerdo</span>
                        </div>
    
                        <div class="form-group col-md-12">
                            <label for="txtBeneficiario">Beneficiario</label>
                            <input class="form-control" placeholder="Beneficiario" id="txtBeneficiario" autocomplete="off">
                            <span id="errorBeneficiario" style="color:red; display:none;">Campo requerdo</span>
                        </div>
    
                        <div class="form-group col-md-12">
                            <label for="txtBeneficiarioDos">Segundo Beneficiario</label>
                            <input class="form-control" placeholder="Responsable" id="txtBeneficiarioDos" autocomplete="off">
                        </div>
    
                        <div class="form-group col-md-12">
                            <label for="txtUbicacion">Ubicación</label>
                            <input class="form-control" placeholder="Ubicación" id="txtUbicacion" autocomplete="off">
                            <span id="errorUbicacion" style="color:red; display:none;">Campo requerdo</span>
                        </div>
    
                        <div class="form-group col-md-12">
                            <label for="txtDescripcion" class="labelTitulo">Descripcion: </label>
                            <textarea class="form-control" rows="3" id="txtDescripcion"></textarea>
                        </div>                    
                    </div>
    
                    <div class="modal-footer">
                        <button type="button" id="btnCerrar" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="button" onclick="validar();" class="btn btn-primary">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection
@extends('layout.main')