
@section('title', 'Criptas')
@section('content')

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left"> 
                <h3>Plain Page</h3>
            </div>

            <button class="btn btn-primary btn-round btnAgregar float-right" data-toggle="modal" data-target="#modalFormulario" onclick="abreModal()" >Agregar</button>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 form-group pull-right top_search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Buscar" id="txtBuscar">
                        <span class="input-group-btn">
                            <button class="btn btn-default" onclick="getData('buscar')" type="button">Buscar</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Plain Page</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>

                            <li>
                                <a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped">
                                    <thead>
                                        <tr style="text-align: center">
                                            <th onclick="ordenar('codigo')" id="codigo" >Código</th>
                                            <th onclick="ordenar('responsable')" id="responsable">Responsable</th>
                                            <th onclick="ordenar('precio')" id="precio">Precio</th>
                                            <th style="width: 40%" onclick="ordenar('descripcion')" id="descripcion">Descripción</th>
                                            <th onclick="ordenar('disponibles')" id="disponibles">Disponibles</th>
                                            <th style="width: 15%">Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="tbody"></tbody>
                                </table>
    
                                <nav aria-label="Page navigation example" class="row" style="float:right;">
                                    <div class="pagination">
                                        <div class="table_info"> Mostrar: </div>
                                        <div style="width: 70px;">
                                            <select class="custom-select" id="show">
                                                <option value="10" selected>10</option>
                                                <option value="25">25</option>
                                                <option value="50">50</option>
                                                <option value="100">100</option>
                                            </select>
                                        </div>
                                        <div class="table_info" id="tableInfo"></div>
                                        <nav aria-label="Page navigation" id="paginacion"></nav>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="modalFormulario" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="tituloModal"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group col-md-4">
                        <label for="txtCodigo">Código</label>
                        <input class="form-control" placeholder="Código" id="txtCodigo" autocomplete="off">
                        <span id="errorCodigo" style="color:red; display:none;">Campo requerdo</span>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="txtLugares">Lugares </label>
                        <input type="number" class="form-control" placeholder="Lugares" id="txtLugares" autocomplete="off">
                        <span id="errorLugares" style="color:red; display:none;">Campo requerdo</span>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="txtPrecio">Precio </label>
                        <input data-validate-minmax="1" type="number" class="form-control" placeholder="Precio" id="txtPrecio" autocomplete="off">
                        <span id="errorPrecio" style="color:red; display:none;">Campo requerdo</span>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="txtResponsable">Responsable</label>
                        <input class="form-control" placeholder="Responsable" id="txtResponsable" autocomplete="off">
                        <span id="errorResponsable" style="color:red; display:none;">Campo requerdo</span>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="txtBeneficiario">Beneficiario</label>
                        <input class="form-control" placeholder="Beneficiario" id="txtBeneficiario" autocomplete="off">
                        <span id="errorBeneficiario" style="color:red; display:none;">Campo requerdo</span>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="txtBeneficiarioDos">Segundo Beneficiario</label>
                        <input class="form-control" placeholder="Responsable" id="txtBeneficiarioDos" autocomplete="off">
                    </div>

                    <div class="form-group col-md-12">
                        <label for="txtUbicacion">Ubicación</label>
                        <input class="form-control" placeholder="Ubicación" id="txtUbicacion" autocomplete="off">
                        <span id="errorUbicacion" style="color:red; display:none;">Campo requerdo</span>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="txtDescripcion" class="labelTitulo">Descripcion: </label>
                        <textarea class="form-control" rows="3" id="txtDescripcion"></textarea>
                    </div>                    
                </div>

                <div class="modal-footer">
                    <button type="button" id="btnCerrar" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" onclick="validar();" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@extends('layout.main')

@section('scripts')
    <script>
        let params  = { activo: true,items_to_show: 10, order_by: 'id' , order: 'desc', page: 1};
        let data    = { url: '/api/criptas', accion: 'add' };
        let cripta  = new Object();
        let criptas = [];

        function getData(){
            params.buscar = null;
            if($("#txtBuscar").val() != '')
                params.buscar = $("#txtBuscar").val();

            $.ajax({
                type: "GET",
                url: data.url + '/find',
                data: params,
                success: function(resp) {
                    criptas  = resp.data;
                    var html = '';

                    resp.data.forEach(function(val,i){
                        html += `<tr> +
                                    <td style="text-align: center"> ${ val.codigo } </td>
                                    <td> ${ val.responsable } </td>
                                    <td style="text-align: center"> ${ val.precio_format } </td>
                                    <td> ${ val.descripcion == null ? '' : val.descripcion } </td>
                                    <td style="text-align: center"> ${ val.disponibles }</td>
                                    <td style="text-align: center"> 
                                        <span>
                                            <button type="button" onclick="detalle('${ val.id }')" title="Ver lugares" class="btn btn-link"><i class="fa fa-list-ul"></i></button>
                                        </span>
                                        <span>
                                            <button type="button" onclick="abreModal('update','${ val.id }')" title="Editar" class="btn btn-link" data-toggle="modal" data-target="#modalFormulario"><i class="fa fa-edit"></i></button>
                                        </span>
                                        <span>
                                            <button type="button" onclick="eliminar('${ val.id }', 'baja')" title="Eliminar" class="btn btn-link"><i class="fa fa-trash-o"></i></button>
                                        </span>
                                    </td>
                                </tr>`;
                    });

                    let pag  = '<ul class="pagination" style="float: right;">';
                    let info = `<span v-text="from" >${ resp.from == null ? 0 : resp.from }</</span> - <span v-text="to" >${ resp.to == null ? 0 : resp.to }</</span> de <span v-text="total" >${ resp.total }</span>`;
                    resp.links.forEach(element => {
                        let disabled = '';
                        let activo   = '';

                        if(element.url === null){
                            disabled = 'disabled';
                        }

                        if(element.active){
                            activo = 'active';
                        }

                        pag += `<li class="page-item ${disabled } ${activo }" disabled><span onclick="cambiaPagina('${ element.url }')" tabindex="0" class="page-link" >${ element.label }</span></li>`;
                    });
                    
                    $("#tableInfo").html('');
                    $("#tbody").html('');
                    $("#paginacion").html('');

                    $("#tableInfo").append(info);
                    $("#tbody").append(html);
                    $("#paginacion").append(pag);
                },
                error: function(resp) {
                    let error = JSON.parse(resp.responseText);
                    respuestaError(error);
                }
            });
        }

        function eliminar(id= 0, accion= 'delete'){
            Swal.fire({
                title: '¿Confirmar?',
                text: "No podrás revertir esta accion",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar'
            }).then((resp) => {
                if (resp.isConfirmed) {
                    $.ajax({
                        url: data.url + '/delete',
                        type: 'POST',
                        data: {"id": id, 'accion': accion },
                        async: false,
                        success: function(data) {
                            Swal.fire('Operación exitosa', '', 'success');
                            getData();
                        },
                        error: function(resp) {
                            let error = JSON.parse(resp.responseText);
                            respuestaError(error);
                        }
                    });
                }
            });
        }

        function ordenar(column){
            let row     =  document.getElementsByClassName("sorting");
            let order   = 'asc';
           
            if(row.length > 0){
                order = row[0].className.includes('asc') ? 'desc' : 'asc';
                const items = document.querySelectorAll('.sorting');
                for (let i = 0; i < items.length; i++) {
                    items[i].className = '';
                }
            }
            document.getElementById(column).classList.add('sorting');
            document.getElementById(column).classList.add(order);
            
            params.order    = order;
            params.order_by = column;
            params.page     = 1;
            getData();
        }

        function cambiaPagina(url = null){
            if(url !== null){
                let pag     = url.split('page=');
                params.page = pag[1];
                getData();
            }
        }

        function abreModal(accion = 'add', id = 0){
            let titulo  =' Agregar Cripta';
            cripta.id   = parseInt(id);
            data.accion = accion;
            limpiaCampos();

            if(accion != 'add'){
                titulo = 'Actualizar Cripta';
                
                criptas.forEach(element => {
                    if(element.id == cripta.id) {
                        $('#txtCodigo').val(element.codigo);
                        $('#txtLugares').val(element.lugares);
                        $('#txtPrecio').val(element.precio);
                        $('#txtResponsable').val(element.responsable);
                        $('#txtBeneficiario').val(element.beneficiario);
                        $('#txtBeneficiarioDos').val(element.codigo);
                        $('#txtUbicacion').val(element.ubicacion);
                        $('#txtDescripcion').val(element.descripcion);
                    }
                });
            }

            $('#tituloModal').text(titulo);
            console.log(accion);
        }

        function detalle(id= 0){
            window.location.href = "/criptas/detalles/" + id;
        }

        function validar(){
            let response = true;
            
            cripta.codigo       = $('#txtCodigo').val();
            cripta.lugares      = $('#txtLugares').val();
            cripta.precio       = $('#txtPrecio').val();
            cripta.responsable  = $('#txtResponsable').val();
            cripta.beneficiario = $('#txtBeneficiario').val();
            cripta.beneficiario_dos = $('#txtBeneficiarioDos').val();
            cripta.ubicacion    = $('#txtUbicacion').val();
            cripta.descripcion  = $('#txtDescripcion').val();
            //cripta.ocupado      =    

            if(cripta.codigo == ''){
                $('#errorCodigo').show();
                response = false;
            }
            else{
                $('#errorCodigo').hide();
            }

            if(cripta.lugares == ''){
                $('#errorLugares').show();
                response = false;
            }
            else{
                $('#errorLugares').hide();
                cripta.lugares  = parseFloat(cripta.lugares);
            }

            if(cripta.precio == ''){
                $('#errorPrecio').show();
                response = false;
            }
            else{
                cripta.precio   = parseInt(cripta.precio);
                $('#errorPrecio').hide();
            }

            if(cripta.responsable == ''){
                $('#errorResponsable').show();
                response = false;
            }
            else{
                $('#errorResponsable').hide();
            }

            if(cripta.beneficiario == ''){
                $('#errorBeneficiario').show();
                response = false;
            }
            else{
                $('#errorBeneficiario').hide();
            }

            if(cripta.ubicacion == ''){
                $('#errorUbicacion').show();
                response = false;
            }
            else{
                $('#errorUbicacion').hide();
            }

            if(response)
                guardar();
        }

        function guardar(){
            let method = 'POST';
            
            if(data.accion != 'add')
                method = 'PUT'
           
            $.ajax({
                type: method,
                url: data.url,
                data: cripta,
                success: function(resp){
                    setTimeout(() => {
                        $("#btnCerrar" ).click();
                        getData();
                    }, 1000);

                    Swal.fire('Operación exitosa', '', 'success');
                },
                error: function(resp) {
                    let error = JSON.parse(resp.responseText);
                    respuestaError(error);
                }
            });
        }

        function limpiaCampos(){
            $('#txtCodigo').val('');
            $('#txtLugares').val('');
            $('#txtPrecio').val('');
            $('#txtResponsable').val('');
            $('#txtBeneficiario').val('');
            $('#txtBeneficiarioDos').val('');
            $('#txtUbicacion').val('');
            $('#txtDescripcion').val('');
        }
        
        function respuestaError(error){
            console.log(error);
            let mensaje = "Se presento un erro favor de intentar de nuevo";
        
            if(typeof(error.error) === 'string'){
                mensaje = error.error;
            }
            else{
                let errores = Object.values(error.error);
                mensaje = ``;

                for (var i = 0; i < errores.length; i++) {
                    mensaje+= `* ` + errores[i] + `<br>`;
                }
            }
            Swal.fire({
                icon: 'error',
                title: 'Error',
                html: mensaje
            });
        }

        /******* Eventos ************/
        $(function() {
            getData();
        });

        $( "#txtBuscar").keypress(function(event) {
            if (event.which == 13) {
                data.page = 1;
                getData();
            }
        });

        $("#show").change(function(){
            data.items_to_show = $('select[id=show]').val();
            data.page = 1;
            getData();
        });


    </script>
@endsection