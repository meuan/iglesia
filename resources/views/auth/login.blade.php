<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<title>Iglesia</title>

    <!--  BOOTSTRAP -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <!-- Fontawesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
	<!-- CSS Manuel -->
	<link rel="stylesheet" href="{{ asset('css/manuel.css') }}">
</head>
<body class="body-login">
	<div class="text-center">
		<div class="col-sm-10 main-section">
			<div class="divFormLogin">
				<div class="col-12 user-img">
					<img  class="imgAvatar" src="{{ asset('img/login.jpeg') }}">
				</div>
				
				<form action="/auth/login" method="POST" id="frmLogin" class="col-md-12 formLogin">
					<h1 style="color: black; margin-bottom: 40px;">Inicio de sesion</h1>
					@csrf
					<div class="form-group inputLogin">
						@if($errors->any())
							<label class="text-center errorLogin" >{{ $errors->first() }} </label>
						@endif
					</div>
					
					<div class="form-group inputLogin" id="divUsuario" style="margin-bottom: 40px;">
						<input placeholder="Usuario" class="form-control" name="username" id="username" autocomplete="off" value="{{ old('username') }}" autofocus/>
						<span class="errorLogin" id="errorUsuario" style="display: none;"></span>
					</div>

					<div class="form-group inputLogin"  id="divPass" style="margin-bottom: 40px;">
						<input placeholder="Contraseña" class="form-control" name="password" id="password" type="password"/>
						<span class="errorLogin" id="errorPassword" style="display: none;">La contraseña es requerida.</span>
					</div>

					<button class="btn btn-primary btnLogin" type="button" id="btnLogin" onclick="login()" ><i class="fas fa-sign-in-alt"></i> Ingresar</button>
				</form>
				<div class="col-md-12 recordar">
					<a href="#"> Recordar contraseña</a>
				</div>
			</div>
		</div>
	</div>

	<!--JQUERY-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	 <!--  BOOTSTRAP -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<!-- Fontawesome-->
	<script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
	
	<script>
		$(document).ready(function(){     
			$("#password").keypress(function(e) {
				if(e.which == 13) {
					login();
				}
			});
		});

		function login(){
			$("#errorLogin").hide();

			if(validaciones() !== true )
				return;

			document.getElementById("frmLogin").submit();
		}

		function validaciones(){
			let success = true;
			$("#errorUsuario").hide();
			$("#errorPassword").hide();
			$("#btnLogin").text("");
			$("#btnLogin").append(`<i class="fas fa-spinner fa-pulse fa-2x" id="cargando"></i>`);

           	if($("#username").val() == ""){
				success = false;
				$("#errorUsuario").text("El campo usuario es requerido.");
				$("#errorUsuario").show();
		   	}else{
				const strongRegex = /^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/;
				if( !strongRegex.test($("#username").val())) {
					success = false;
					$("#errorUsuario").text("El usuario debe de ser un correo.");
					$("#errorUsuario").show();
				}
			}				

			if($("#password").val() == ""){
				success = false;
				$("#errorPassword").show();
				
			}

			if(!success){
				$("#btnLogin").text("Iniciar Sesión");
				$("#cargando").remove();
			}

            return success;
        }
	</script>
</body>
</html>