@section('title', 'Criptas')
@section('content')
    <cripta-create  />
    {{-- <cripta-index /> --}}

@endsection
@extends('layout.main')

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{{ asset('template/vendors/validator/multifield.js') }}"></script>
    <script src="{{ asset('template/vendors/validator/validator.js') }}"></script>

    <script>
        // initialize a validator instance from the "FormValidator" constructor.
        // A "<form>" element is optionally passed as an argument, but is not a must
        var validator = new FormValidator({
            "events": ['blur', 'input', 'change']
        }, document.forms[0]);

        // on form "submit" event
        document.forms[0].onsubmit = function(e) {
            var submit = true,
                validatorResult = validator.checkAll(this);
          
            return !!validatorResult.valid;
        };

        // on form "reset" event
        document.forms[0].onreset = function(e) {
            validator.reset();
        };

    </script>
@endsection