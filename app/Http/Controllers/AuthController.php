<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Funcion para autentificarse 
     * @param Request   $request
     * @return Boolean
     */
    public function login(Request $request){
        if(Auth::attempt(['correo' => $request->username, 'password' => $request->password, 'activo' => true ])){
            request()->session()->regenerate();
            return redirect()->route('criptas');
        }
        
        return back()
            ->withErrors(['username' => trans('auth.failed')])
            ->withInput(request(['username'])); 
    }

    /**
     * Elimina las session del usuario autenticado 
     * @param Request   $request
     * @return Boolean
     */ 
    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->route('login');
    }   
}
