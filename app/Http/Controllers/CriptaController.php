<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Cripta;
use Illuminate\Http\Request;
use App\Models\DetalleCripta;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\CriptaValidator;

class CriptaController extends Controller
{
    protected $validate;
    public function __construct() {
        $this->validate = new CriptaValidator();
    }

    /**
     * Rediercciona a la vista del listado de las criptas
     * 
     * @return Response
     */
    public function index(){
        return view('criptas.index');
    }

    /**
     * Crea un registro en la db
     * @param Request   $request
     * @return Response
     */
    public function store(Request $request){
        $validation = $this->validate->cripta($request, 'add');
        
        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);
        
        $data = $request->all();
        $data['disponibles'] = $request->lugares;
        $item = Cripta::create($data);
        
        return response()->json(['item'=> $item], 200);
    }

    /**
     * Retorna un Objeto tipo Usuario, si este existe en la db
     * @param Integer   $id
     * @return Object
     */
    public function get($id){
        $item = Cripta::find($id);
        
        if(is_null($item))
            return response()->json( ['error'=> "No se encontro el registro."], 403);
        $item->detalles;
        return response()->json($item, 200);
    }

    /**
     * Actualiza un registro en la db
     * @param Request   $request
     * @return Boolean
     */
    public function update(Request $request){
        $response = array('message' => 'Error');
        $codigo = 403;
        
        try{
            $validation = $this->validate->cripta($request,'update');
            if( $validation  !== true)
                return response()->json(['error'=> $validation->original], 403);

            $data     = $request->all();
            $ocupados = DetalleCripta::where('cripta_id',$request->id)->count();

            if($ocupados > $data['lugares'])
                return response()->json(['error'=> 'El numero de lugares ocupados es mayor al disponible.'], 403);

            Cripta::find($request->id)->first()->update($data);
            $response["message"] = "Operacion Exitosa";
            $codigo = 200;
        }
        catch(Exception $ex){
            $response["message"] = $ex->getMessage();
        }
        return response()->json($response, $codigo);
    }

    /**
     * Elimina un registro en la db
     * @param Request   $request
     * @return Object
     */
    public function delete(Request $request){
        $response = array('message' => 'Error');
        $codigo = 403;
        
        try{
            $item = Cripta::find($request->id);
            if(is_null($item))
                return response()->json( ['error'=> "No se encontro el registro."], 403);

            if($request->accion == 'baja'){
                $item->activo = false;
                $item->save();
            }
            else
                $item->delete();

            $response["message"] = "Operacion Exitosa";
            $codigo = 200;
        }    
        catch(Exception $ex){
            $response["message"] = $ex->getMessage();
        }
        return response()->json($response, $codigo);
    }

    /**
     * Retorna un json de criptas
     * @param Request $request
     * @return Response
     */
    public function find(Request $request){
        $items      = [];
        $order      = 'desc';
        $order_by   = 'c.id';
        $show       = 10;

        $validation = $this->validate->find($request);
        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);

        $query = DB::table('cripta as c') ->select('c.*');

        if(!is_null($request->buscar)){
            $query->where('c.codigo','ilike', '%'.$request->buscar.'%')
                ->orWhere('c.responsable','ilike', '%'.$request->buscar.'%')
                ->orWhere('c.beneficiario','ilike', '%'.$request->buscar.'%')
                ->orWhere('c.beneficiario_dos','ilike', '%'.$request->buscar.'%');
        }

        if(!is_null($request->activo)){
            $activo =  $request->activo == 'true' ? true : false;
            $query->where('c.activo', $activo);
        }

        /* Parametros para la paginanacion y el orden */
        if(!is_null($request->order))
            $order = $request->order;

        if(!is_null($request->order_by))
            $order_by = $request->order_by;
        
        if(!is_null($request->order_by)){
            $order_by = 'c.'.$request->order_by;
        }
        $query->orderBy($order_by, $order); 
       
        if(is_null($request->paginate) || $request->paginate == "true" ){
            if(!is_null($request->items_to_show))
                $show = $request->items_to_show;
            
            $items = $query->paginate($show);
            foreach($items as $item){
                $item->precio_format = "$".number_format(round($item->precio, 2, PHP_ROUND_HALF_UP), 2, ".", ",");
            }
        }
        else{
            $items = $query->get();   
            foreach($items as $item){
                $item->precio_format = "$".number_format(round($item->precio, 2, PHP_ROUND_HALF_UP), 2, ".", ",");
            }         
            return response()->json(["data" => $items], 200);
        }
        
        return response()->json($items, 200);
    }
}
