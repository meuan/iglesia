<?php

namespace App\Http\Controllers;

use App\Models\Cripta;
use Illuminate\Http\Request;

class CriptaDetalleController extends Controller
{
    /* protected $validate;
    public function __construct() {
        $this->validate = new CriptaValidator();
    } */

    /**
     * Retorna la vista del detalle de una cripta en especifico
     * @param int $id
     * @return view
     */
    public function individual(int $id){
        $cripta = Cripta::find($id);
        return view('cripta-detalle')->with([ 'cripta' => $cripta]);
    }
}
