<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Http\FormRequest;

class CriptaValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * Valida los campos necesarios para crear o actualizar un registro
     * @param Request   $request
     * @param string    $accion
     * @return Response
     */
    public function cripta(Request $request, $accion= 'add'){
        $params = [
            'codigo'        => 'required|string|max:20',
            'responsable'   => 'required|string|max:200',
            'beneficiario'  => 'required|string|max:200',
            'beneficiario_dos'   => 'nullable|string|max:200',
            'precio'        => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'ubicacion'     => 'required|string|max:200',
            'descripcion'   => 'nullable|string',
            'lugares'       => 'required|integer',
            'ocupado'       => 'nullable|boolean',
        ];

        if($accion != 'add')
            $params = array_merge([ "id" => 'required|integer|exists:cripta,id'], $params);

        $validator = Validator::make($request->all(), $params);
       
        if ($validator->fails())
            return response()->json($validator->errors());
        
        return true;
    }

    public function find(Request $request){
        $validator = Validator::make($request->all(), [
            'order'     => ['nullable','string', Rule::in(['asc', 'desc'])],
            'order_by'  => ['nullable','string', Rule::in(['id', 'codigo','responsable','beneficiario', 'precio', 'descripcion', 'disponibles', 'general' ])],
            'show'      => 'nullable|integer',
        ]);
       
        if ($validator->fails()) 
            return response()->json($validator->errors());

        return true;
    }

    public function delete(Request $request){
        $validator = Validator::make($request->all(), [
            'id'      => 'required|integer|exists:cripta,id',
            'accion'  => ['required','string', Rule::in(['delete', 'baja'])],
        ]);
       
        if ($validator->fails()) 
            return response()->json($validator->errors());

        return true;
    }
}
