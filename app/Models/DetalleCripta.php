<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DetalleCripta extends Model
{
    use HasFactory;

    protected $table = 'cripta_detalle';
    
    protected $fillable =  [
        'cripta_id',
        'codigo',
        'difunto',
        'fecha_ingreso',
        'estatus',
        'comentarios'
    ];

    public function cripta() : BelongsTo{
        return $this->belongsTo(Cripta::class);
    }
}
