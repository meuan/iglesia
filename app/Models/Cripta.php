<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Cripta extends Model
{
    use HasFactory;
    protected $table = 'cripta';

    protected $fillable =  [
        'codigo',
        'responsable',
        'beneficiario',
        'beneficiario_dos',
        'precio',
        'ubicacion',
        'descripcion',
        'disponibles',
        'lugares',
        'ocupado',
        'baja'
    ];

    public function detalles() : HasMany{
        return $this->hasMany(DetalleCripta::class);
    }
}
