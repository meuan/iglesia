<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CriptaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'codigo'        => $this->faker->numberBetween(100,300),
            'responsable'   => $this->faker->name(),
            'beneficiario'  => $this->faker->name(),
            'beneficiario_dos'=> $this->faker->name(),
            'precio'        => $this->faker->numberBetween(2500, 5000),
            'ubicacion'     => $this->faker->word(),
            'descripcion'   => $this->faker->text(),
            'disponibles'   => $this->faker->numberBetween(1,10),
            'lugares'       => $this->faker->numberBetween(1,10),
            'ocupado'       => $this->faker->boolean(),
            
        ];
    }
}
