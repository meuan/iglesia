<?php

namespace Database\Seeders;

use App\Models\Cripta;
use Illuminate\Database\Seeder;

class CriptasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cripta::factory(100)->create();
    }
}
