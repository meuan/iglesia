<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCriptaDetalleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cripta_detalle', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cripta_id')->index();
            $table->string('codigo', 10)->nullable();
            $table->string('difunto', 200);
            $table->date('fecha_ingreso')->nullable();
            $table->enum('estatus', ['disponible', 'ocupado','ausente'])->default('disponible');
            $table->text('comentarios')->nullable();
            $table->timestamps();

            $table->foreign('cripta_id')->references('id')->on('cripta')
                    ->onDelete("cascade")
                    ->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cripta_detalle');
    }
}
