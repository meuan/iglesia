<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCriptaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cripta', function (Blueprint $table) {
            $table->id();
            $table->string('codigo', 10);
            $table->string('responsable', 200);
            $table->string('beneficiario', 200);
            $table->string('beneficiario_dos', 200)->nullable();
            $table->double('precio', 8,2)->default(0);
            $table->string('ubicacion', 200)->nullable();
            $table->text('descripcion')->nullable();
            $table->integer('disponibles')->default(0);
            $table->integer('lugares')->default(1);
            $table->boolean('ocupado')->default(false);
            $table->boolean('activo')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cripta');
    }
}
