<?php

use App\Http\Controllers\CriptaController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'criptas'], function () {
    Route::post('/', [CriptaController::class, 'store']);
    Route::put('/', [CriptaController::class,'update']);
    Route::post('/delete', [CriptaController::class, 'delete']);
    Route::get('/find', [CriptaController::class, 'find']);
    Route::get('/{id}', [CriptaController::class, 'get']);
});