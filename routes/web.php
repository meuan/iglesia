<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CriptaController;
use App\Http\Controllers\CriptaDetalleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'auth.login')->name('login');
Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', [AuthController::class, 'login'])->middleware('guest');
    Route::post('/logout', [AuthController::class,'logout'])->name('logout');
});

Route::view('/criptas', 'criptas')->name('dashboard')->middleware('auth');

Route::group(['prefix' => 'criptas'], function() {
    Route::get('/', [CriptaController::class, 'index'])->name('criptas');
    Route::get('/detalles/{id}', [CriptaDetalleController::class,'individual'])->name('individual');
    
   /*  Route::view('/', 'criptas')->name('criptas')->middleware('auth');;
    */
});

Route::view('/agenda', 'agenda')->name('agenda');

Route::view('/editor', 'editor')->name('editor');
